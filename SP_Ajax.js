// Methods = get,post,put,merge,delete
//urls site collection:  http://server/site/_api/site To access a specific site : http://server/site/_api/web

$.fn.SPList = 
function (options){
var opt = $.extend ({},{
listName: "",
properties:function(string)
{ string.replace("'","\'")
    return[string]}, // syntax ["'Title':'Testing'","'Description':'Testing'"]
templateID:100,//default - custom list   
listData:{} 
/*common list templates:
custom list: 100
document library: 101
events(calendar): 106
tasks: 107
full ref: https://docs.microsoft.com/en-us/dotnet/api/microsoft.sharepoint.splisttemplatetype?view=sharepoint-server (not all apply to 2013)
*/
},options);
return { opt,
    createList : function(){
    $.ajax({url:  _spPageContextInfo.webAbsoluteUrl +  '/_api/web/lists',
        type: "POST",
        data: "{ '__metadata': { 'type': 'SP.List' },"+opt.properties+",'BaseTemplate':"+opt.templateID+"}",
        headers: 
        {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()}})
        .done(function(data){ console.log("Success "+ JSON.stringify({data}));
        opt.listName = data.d.Title;
        opt.listData = data.d;
        }).fail(function(){console.log("Fail")})},

    deleteList : function(){
            $.ajax({
            url:  _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/",
            type: "POST",
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "IF-MATCH": "*",
                "X-HTTP-Method": "DELETE"
            }}).done(function(){ console.log("Success");
            }).fail(function(){console.log("Fail")})
            },
    // change list title or property etc.
    updateList : function(){ 
                $.ajax({
                    url:  _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/",
                    type: "POST",
                    data:"{ '__metadata': { 'type': 'SP.List' },"+opt.properties+"}",
                    headers: {
                        "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                        "X-HTTP-Method": "MERGE",
                        "IF-MATCH": "*",
                        "accept": "application/json;odata=verbose",
                        "content-type": "application/json;odata=verbose"
                        }
                }).done(function(data){ console.log("Success");
            }).fail(function(data){console.log("Fail" + data)})
            } ,
// get list properties
    getList : function(){
        $.ajax(
            {
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/",
            method: "GET",
            datamethod: "json",
            headers : { "Accept": "application/json; odata=verbose" }
            }).done(function(data){ console.log("Success"+ JSON.stringify({data}));
            opt.listData=data.d;
        }).fail(function(){console.log("Fail")})
                                }
}}   
  
//List Item Functions//

$.fn.SPItem = 
function (options){
var opt = $.extend ({},{
listName: "",
properties:function(string)
{ string.replace("'","\'")
    return[string]}, //item syntax ["'Title':'Testing'","'Field':'Testing'"]
itemID: null,
fileName:null,
body:"",
itemData:{}
},options); return {opt,
getListItems : function () {
    var url;
    if (opt.itemID === null){url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/items"}
    else {url =_spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")"}
    $.ajax(
        {
        url: url,
        type: "GET",
        dataType: "json",
        headers : { "accept": "application/json; odata=verbose" }}).done(function(data){ console.log("Success " + url + " " + JSON.stringify(data.d));
        opt.itemData= data.d;
        }).fail(function(){console.log("Fail")})
    },

createListItem : function(){
    $.ajax({ 
    url: _spPageContextInfo.webAbsoluteUrl +  "/_api/web/lists/getbytitle('"+opt.listName+"')/items",
    type: "POST",
    data:"{ '__metadata': { 'type': 'SP.Data."+opt.listName+"ListItem' },"+opt.properties+"}",
    headers:{
     "X-RequestDigest": $("#__REQUESTDIGEST").val(),
    "accept": "application/json;odata=verbose",
    "content-type": "application/json;odata=verbose"}})
.done(function(data){ console.log("Success")
opt.itemID=data.d.ID;
opt.itemData=data.d;
}).fail(function(){console.log("Fail")})
},

updateListItem:  function(){
    $.ajax({ 
    url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")",
    type: "POST",
    data:"{ '__metadata': { 'type': 'SP.Data."+opt.listName+"ListItem' },"+opt.properties+"}",
    headers: {
        "X-RequestDigest": $("#__REQUESTDIGEST").val(),
        "IF-MATCH": "*",
        "accept": "application/json;odata=verbose",
        "content-type": "application/json;odata=verbose",
        "X-HTTP-Method": "MERGE"
        }
    }).done(function(){ console.log("Success");
    }).fail(function(){console.log("Fail")})
    },

deleteListItem: function(){
        $.ajax({ 
        url:  _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")",
        type: "POST",
        headers:{
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "X-HTTP-Method":"DELETE",
            "IF-MATCH": "*",}})
        .done(function(){ console.log("Success");
        }).fail(function(){console.log("Fail")})
        },

getitemAttachments: function (){
    var url;
    if (opt.fileName === null){url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")/AttachmentFiles/"}
    else {url =_spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")/AttachmentFiles('"+opt.fileName+"')/$value"}
$.ajax({
    url: url,
    type: "GET",
    headers:{
          "accept": "application/json;odata=verbose"}
})
.done(function(data){ console.log("Success" +url + " " + JSON.stringify({data}))
}).fail(function(data){console.log("Fail" +url + " " + JSON.stringify({data}))})
},

createItemAttachment: function(){
    $.ajax({
url: _spPageContextInfo.webAbsoluteUrl +  "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")/AttachmentFiles/add(FileName='"+opt.fileName+"')",
type: "POST",
data: body,
headers: {
    "X-RequestDigest": $("#__REQUESTDIGEST").val()
    }})
}, //currently only works with .txt in order to use office docs , you need to create doc locally and "upload"

deleteItemAttachment : function(){
    $.ajax({
url: _spPageContextInfo.webAbsoluteUrl +  "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")/AttachmentFiles('"+opt.fileName+"')",
type: "POST",
headers: {
    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "X-HTTP-Method":"DELETE",
            "IF-MATCH": "*",}})
},

updateItemAttachment: function(){
        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl +  "/_api/web/lists/getbytitle('"+opt.listName+"')/items("+opt.itemID+")/AttachmentFiles/add(FileName='"+opt.fileName+"')",
            type: "POST",
            data: opt.body,
            headers: {
                "X-HTTP-Method":"PUT",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),}})
            } //currently only works with .txt willl OVERWRITE  

        }}     

/*
List Field Functions

Common Field Types : 
29 - allDayEvent
19- attachments
8- bool
17-calculated
6-choice
10-currency
4 - dateTime
9- number
7- lookup
15 - multiselect
2- text
20-user
full ref: https://docs.microsoft.com/en-us/previous-versions/office/sharepoint-visio/jj245640%28v%3doffice.15%29
*/

/* Commmon Available Properties: 
CanBeDeleted 
DefaultValue
Description
EnforceUniqueValues
FieldTypeKind -- made this seperate so you can easily see what type of field it is
InternalName
Required
ReadOnlyField


Full Ref: https://docs.microsoft.com/en-us/previous-versions/office/developer/sharepoint-rest-reference/dn600182%28v%3doffice.15%29
*/

$.fn.SPField = function(options) {
    var opt = $.extend({},{
    listName:"",
    fieldName:"",
    properties:function(string)
              {string.replace("'","\'")
    return[string]}, //current syntax ex: ["'Title':'Testing'","'Description':'Testing'"] -- see above list of properties
    fieldType:2, // default = Text
    fieldData:null,
    property:null  },options);
return {opt,
    createListField : function (){
        
        $.ajax({    
        url: _spPageContextInfo.webAbsoluteUrl  + "/_api/web/lists/GetByTitle('"+opt.listName+"')/fields",
        type:"POST",
        data: "{ '__metadata': { 'type': 'SP.Field' }, "+opt.properties+",'FieldTypeKind':"+opt.fieldType+"}",
        headers: {           
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
          }
        }).done(function(data){ console.log("Success")
        opt.fieldName=data.d.Title;
        opt.fieldData= data.d;
    }).fail(function(){console.log("Fail")})}, // only supports one field at a time currently 

    updateListField: function(){
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl  + "/_api/web/lists/getbytitle('"+opt.listName+"')/fields/getbytitle('"+opt.fieldName+"')", 
        type: "POST",
        data: "{ '__metadata': { 'type': 'SP.Field' }, "+opt.properties+" }",
        headers: { 
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
          "content-type": "application/json;odata=verbose",
          "X-HTTP-Method": "MERGE"
        },
     }).done(function(){ console.log("Success");
    }).fail(function(){console.log("Fail")});

},
    deleteListField:function (){
    $.ajax({
        url:_spPageContextInfo.webAbsoluteUrl  + "/_api/web/lists/getbytitle('"+opt.listName+"')/fields/getbytitle('"+opt.fieldName+"')", 
        type: "POST",
        headers: { 
        "X-RequestDigest": $("#__REQUESTDIGEST").val(),
          "X-HTTP-Method": "DELETE",
          "IF-MATCH": "*"
        },
      }).done(function(){ console.log("Success");
    }).fail(function(){console.log("Fail")});
},
    getFieldProps:  function(){
      var url;
      if (opt.property===null){
          url = _spPageContextInfo.webAbsoluteUrl +"/_api/web/lists/getbytitle('"+opt.listName+"')/fields/getbytitle('"+opt.fieldName+"')"}
      else {url =  _spPageContextInfo.webAbsoluteUrl +"/_api/web/lists/getbytitle('"+opt.listName+"')/fields/getbytitle('"+opt.fieldName+"')/"+opt.property+""}
    $.ajax({
        url:url,
        type: "GET",
        headers: { "accept": "application/json; odata=verbose" },
    }).done(function(data){ console.log("Success" + " "+ JSON.stringify({data}))
    opt.fieldData=data.d;
    if (opt.property ===null) {opt.fieldType=data.d.FieldTypeKind};
    }).fail(function(){console.log("Fail")});
    }

}}     

//Folder (Document Library) Functions//

$.fn.spFolder = function(options) {
    var opt = $.extend({},{
    libraryName:"",
    folderName:"",
    },options)
return{opt,
    getFolder:function(){
        $.ajax({
        url:_spPageContextInfo.webAbsoluteUrl+ "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')",
        type: "GET",
        headers:{
            "accept": "application/json;odata=verbose"
        }}).done(function(data){ console.log("Success" + " "+ JSON.stringify({data}));
        opt.folderName=data.d.Name;
        }).fail(function(){console.log("Fail"+ " "+ url)})
        },
        createFolder:function(){
            $.ajax({url:_spPageContextInfo.webAbsoluteUrl + "/_api/web/folders",
            type: "POST",
            data: "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': '"+opt.libraryName+"/"+opt.folderName+"'}",
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "accept": "application/json;odata=verbose",
                "content-type": "application/json;odata=verbose"}}).done(function(){ console.log("Success");
            }).fail(function(){console.log("Fail")})
        },
        updateFolder:function(){
        $.ajax({url:_spPageContextInfo.webAbsoluteUrl+ "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')/ListItemAllFields",
        type: "POST",
        data: "{ '__metadata': { 'type': 'SP.Data."+opt.libraryName+"Item' }, 'Title': '"+opt.folderName+"' }",
        headers: {
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "IF-MATCH":  "*",
            "X-HTTP-Method":"MERGE",
            "content-type": "application/json;odata=verbose"}}).done(function(data){ console.log("Success"+" "+ JSON.stringify({data}));
        }).fail(function(){console.log("Fail")})
        }, /// needs more work 
       deleteFolder: function (){
            $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+opt.folderName+"')",
            type: "POST",
            headers: 
                {
                 "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "IF-MATCH":  "*",
                "X-HTTP-Method":"DELETE"}}).done(function(){ console.log("Success");
            }).fail(function(){console.log("Fail")})
        }
   }}




//File Functions//

$.fn.SPFile = function(options) {
    var opt = $.extend({},{
    libraryName:"",
    folderName:null,
    fileName:null,
    fileData:{},
    property:null,
    body:"",
    comment:"Checking in this file.",
    },options)
    return{opt,
        getFiles:function (){
            var url;
            if (opt.folderName===null && opt.fileName === null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')/Files"}
            else if (opt.folderName===null && opt.fileName !==null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')/Files('"+opt.fileName+"')" }
            else if (opt.folderName!==null && opt.fileName ===null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+opt.folderName+"')/Files" }
            else if (opt.folderName!==null && opt.fileName !==null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+opt.folderName+"')/Files('"+opt.fileName+"')" }
            $.ajax({
        url: url,
        type: "GET",
        headers:{
            "accept": "application/json;odata=verbose"
        }}).done(function(data){ console.log("Success "+ url+ data)
        opt.fileData=data.d;
        }).fail(function(){console.log("Fail")})
        },
        createFile : function(){
            var url;
            if (opt.folderName===null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')/Files/add(url='"+opt.fileName+"' , overwrite = true)"}
            else {url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+opt.folderName+"')/Files/add(url='"+opt.filename+"' , overwrite = true)"}
            $.ajax({
            url: url,
            type: "POST",
            data: opt.body ,
            headers: 
                {
                 "X-RequestDigest": $("#__REQUESTDIGEST").val(),}})
            .done(function(){ console.log("Success");
            }).fail(function(){console.log("Fail")})
        }, //in order to use office docs , you need to create doc locally and "upload" // works if there is no content
        
        deleteFile : function(){
            var url;
            if (opt.folderName===null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')/Files('"+opt.fileName+"')"}
            else {url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+opt.folderName+"')/Files('"+opt.fileName+"')"}
            $.ajax({
            url:url,
            type: "POST",
            headers: 
                {
                 "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "IF-MATCH":  "*",
                "X-HTTP-Method":"DELETE"}}).done(function(){ console.log("Success");
            }).fail(function(){console.log("Fail")})
        },
        
        updateFile:function(){
            var url;
               if (opt.folderName===null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')/Files('"+opt.fileName+"')/$value"}
               else {url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+opt.folderName+"')/Files('"+opt.fileName+"')/$value"}
               $.ajax({url:url,
           type: "POST",
           data: opt.body ,
           headers: {
               "X-RequestDigest": $("#__REQUESTDIGEST").val(),
               "X-HTTP-Method":"PUT",}})
           .done(function(){ console.log("Success");
           }).fail(function(){console.log("Fail")})
           }, //only works for text files
        
        fileCheckOut : function(){
            var url;
            if (opt.folderName===null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+libraryName+"')/Files('"+fileName+"')/CheckOut()"}
            else {url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+folderName+"')/Files('"+fileName+"')/CheckOut()"}
            $.ajax({
                url: url,
                type: "POST",
                headers: 
                    {
                     "X-RequestDigest": $("#__REQUESTDIGEST").val(),}})
                .done(function(){ console.log("Success");
                }).fail(function(){console.log("Fail")})
            },
        
            fileCheckIn: function(){
                var url;
                if (opt.folderName===null){url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"')/Files('"+opt.fileName+"')/CheckIn(comment='"+opt.comment+"', checkintype=0)"}
                else {url = _spPageContextInfo.webAbsoluteUrl  + "/_api/web/GetFolderByServerRelativeUrl('"+opt.libraryName+"/"+opt.folderName+"')/Files('"+opt.fileName+"')/CheckIn(comment='"+opt.comment+"', checkintype=0)"}
            $.ajax({
                url: url,
                type: "POST",
                headers: 
                    {
                     "X-RequestDigest": $("#__REQUESTDIGEST").val(),}})
                .done(function(){ console.log("Success");
                }).fail(function(){console.log("Fail")})
            }
        

  
    }}
    /// Site Functions //         
$.fn.SPSite = function(options){
var opt = $.extend({},{
siteTitle:'',
siteURL: function(){return this.siteTitle;}, //set to equal site title by default
siteDescription :'',
siteTemplate:"sts#0", //default - team site 
/* common site templates: blog - blog#0 , dev - dev#0, projectsite - projectsite#0 */ 
siteData:{}
},options);
return {opt,
        getSite: function(){ 
            $.ajax({
            
            url: _spPageContextInfo.webAbsoluteUrl  +"/"+ opt.siteTitle + "/_api/web",
            type: "GET",
            headers:{
                "accept": "application/json;odata=verbose"
            }}).done(function(data){ console.log("Success "+ JSON.stringify({data}))
            opt.siteData = data.d;
            }).fail(function(){console.log("Fail")})
        },
        createSite : function () {
                $.ajax({        
                        url:  _spPageContextInfo.webAbsoluteUrl  + "/_api/web/webinfos/add",        
                        type: "POST",       
                        data: JSON.stringify(
                            {'parameters': {
                                '__metadata':  {'type': 'SP.WebInfoCreationInformation' },
                                'Url': opt.siteURL,
                                'Title': opt.siteTitle,
                                'Description': opt.siteDescription,
                                'Language':1033,
                                'WebTemplate':opt.siteTemplate,
                                'UseUniquePermissions':false}
                            }
                        ),
                        headers: {
                            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                            "accept": "application/json; odata=verbose",
                            "content-type":"application/json;odata=verbose"
                        }}).done(function(){ console.log("Success");
                    }).fail(function(){console.log("Fail")})
                },
        deleteSite: function (){
            $.ajax({
                url:  _spPageContextInfo.webAbsoluteUrl  +"/"+ opt.siteTitle + "/_api/web",
                type: "POST",
                headers: {
                            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                            "IF-MATCH":  "*",
                            "X-HTTP-Method":"DELETE",                   
                            "accept": "application/json; odata=verbose",
                            "content-type":"application/json;odata=verbose"
                }}).done(function(){ console.log("Success");
            }).fail(function(){console.log("Fail")})
        },
        updateSite: function () { $.ajax({
            url:  _spPageContextInfo.webAbsoluteUrl  +"/"+ siteTitle + "/_api/web",
            type: "POST",
            data: JSON.stringify(
                {
                    '__metadata':  {'type': 'SP.Web' },
                    'Url': opt.siteURL,
                    'Title': opt.siteTitle,
                    'Description': opt.siteDescription
                }
            ),
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "IF-MATCH":  "*",
                "X-HTTP-Method":"MERGE",                   
                "accept": "application/json; odata=verbose",
                "content-type":"application/json;odata=verbose"
            }}).done(function(){ console.log("Success");
        }).fail(function(){console.log("Fail")})
        }
        }}